package ann

// Neuron types
type transferFunction func(float32) synapse
type summatorFunction func(synapses ...synapse) float32

// neural network structure
type layer []Neuron
