package ann

type synapse struct {
	w float32
	x float32
}

// exciting synapse type
func (s *synapse) exciting() bool {
	return s.w > 0
}

// inhibiting synapse type
func (s *synapse) inhibiting() bool {
	return s.w < 0
}
