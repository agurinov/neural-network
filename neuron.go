package ann

// Neuron blabla
type Neuron struct {
	transfer transferFunction
	summator summatorFunction
}

// NewNeuron - neuron constructor.
// receives:
// offset - threshold synapse of transfer function
// transferFunction - transfer function
// summator - summator logic, may be nil, default summator will bse provided
func NewNeuron(offset synapse, transfer transferFunction, summator summatorFunction) *Neuron {
	defaultSummator := func(synapses ...synapse) float32 {
		// neuron threshold
		T := offset.w * offset.x
		var sum float32
		for _, synapse := range synapses {
			sum += synapse.w * synapse.x
		}
		return sum + T
	}
	if summator == nil {
		summator = defaultSummator
	}
	return &Neuron{
		transfer: transfer,
		summator: summator,
	}
}
